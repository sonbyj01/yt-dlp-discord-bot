import discord
import os
import logging

from dotenv import load_dotenv
from subprocess import STDOUT, check_output, CalledProcessError

BASE_URL = 'https://disc.hson.dev'
DOWNLOAD_DIR = '/downloads'

load_dotenv()

client = discord.Client()

# Set up logging
formatter = logging.Formatter("%(asctime)s:%(name)s: %(levelname)7s - %(message)s", "%Y-%m-%d_%H:%M:%S")

console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
console.setFormatter(formatter)

logfile = logging.FileHandler("bot.log")
logfile.setLevel(logging.INFO)
logfile.setFormatter(formatter)

logger = logging.getLogger("DiscordBot")
logger.setLevel(logging.DEBUG)
logger.addHandler(console)
logger.addHandler(logfile)

@client.event
async def on_ready():
    logger.info('We have logged in as {0.user}'.format(client))

async def waiting(msg):
    logger.info('Downloading ' + msg)

    try:
        logger.info('Attempting script...')
        output = check_output("./script-new.sh {}".format(msg).split(), stderr=STDOUT, timeout=30).decode('utf-8')
        metadata_line = output.split('[Metadata]')[1]
        path = metadata_line.split('\"')[1]
        logger.info('Old Path...\n' + path)

        new_path = ''
        for c in path:
            if c == ' ':
                new_path += '_'
            elif c in 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~:/?#[]@!$&\'()*+,;=':
                new_path += c
        
        os.rename(path, new_path)
        logger.info('New Path...\n' + new_path)

        logger.info('Success...')
        logger.info('Path: ' + new_path)
        logger.info('Output:\n' + output)
        output = new_path
    except CalledProcessError as e:
        logger.info('Failed [CalledProcessError]...')
        output = f"[Error code {e.returncode}]: {e.output.decode()}"
        logger.info('Output:\n' + output)
    except Exception as e:
        logger.info('Failed [Exception]...')
        output = str(e)
        logger.info('Output:\n' + output)
    return output

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith('$download'):
        await message.channel.send('downloading...')
        logger.info('Received download link')

        output = await waiting(message.content.split()[1])
        logger.info('Exited function...')

        await message.channel.send('Output: \n' + BASE_URL + output)
        logger.info('Sent response...')

if __name__ == '__main__':
    client.run(os.getenv('TOKEN'))

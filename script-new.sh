#!/bin/bash
yt-dlp \
    --no-overwrites \
    --add-metadata \
	-f 'bv+ba/b' \
    -o '/downloads/%(title)s.%(ext)s' \
    "${1}"
FROM ubuntu/nginx
ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && \
    # apt upgrade -y && \
    apt install -y ffmpeg python3 python3-pip

RUN mkdir /downloads

RUN mkdir /bot
WORKDIR /bot

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY discord-bot.py .
COPY script-new.sh .
COPY start.sh .
COPY .env .
COPY default /etc/nginx/sites-available/

RUN chmod 0744 discord-bot.py script-new.sh start.sh

ENV TZ="America/New_York"

EXPOSE 80
EXPOSE 443

CMD ["nginx", "-g", "daemon off;"]
CMD bash
ENTRYPOINT ["/bot/start.sh"]

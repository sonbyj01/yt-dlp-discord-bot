# yt-dlp-discord-bot



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:9a4652b7265142c52c73d26fe1d4fb38?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:9a4652b7265142c52c73d26fe1d4fb38?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:9a4652b7265142c52c73d26fe1d4fb38?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/sonbyj01/yt-dlp-discord-bot.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:9a4652b7265142c52c73d26fe1d4fb38?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:9a4652b7265142c52c73d26fe1d4fb38?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:9a4652b7265142c52c73d26fe1d4fb38?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:9a4652b7265142c52c73d26fe1d4fb38?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:9a4652b7265142c52c73d26fe1d4fb38?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:9a4652b7265142c52c73d26fe1d4fb38?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:9a4652b7265142c52c73d26fe1d4fb38?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:9a4652b7265142c52c73d26fe1d4fb38?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:9a4652b7265142c52c73d26fe1d4fb38?https://docs.gitlab.com/ee/user/clusters/agent/)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:9a4652b7265142c52c73d26fe1d4fb38?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Youtube Video Downloader Discord Bot

## Description
Send a youtube link to a discord bot and it will download it for you.
Motivation: I wanted to save EDM live sets just in case they get taken down.

## Badges
I don't know how to do this properly.

![pipline-status](https://gitlab.com/sonbyj01/yt-dlp-discord-bot/badges/main/pipeline.svg)

## Visuals
Maybe later.

## Installation
1. Get a bot secret token [from here](https://realpython.com/how-to-make-a-discord-bot-python/#creating-a-bot)
2. Add the bot to a discord server you have privilege [using this section](https://realpython.com/how-to-make-a-discord-bot-python/#adding-a-bot-to-a-guild)
3. Pull this repository
```
git pull https://gitlab.com/sonbyj01/yt-dlp-discord-bot.git
```
4. Add an .env file and fill the key to "TOKEN" with the bot secret token from Step 1
```
TOKEN={BOT-SECRET-TOKEN}
```
5. Build the docker image via docker-compose
```
docker-compose up -d --build
```

## Usage
```
$download {url}
```
When you download a video now, it will be saved in "downloads"

## Support
Just leave an issue or something.

## Roadmap
I don't know, whenever I get bored and decide to work on it again.

## Contributing
![do-something](img/do-something.png)

## Authors and acknowledgment
You're welcome.

## License
[MIT License](LICENSE)
Whatever that means.

## Project status
TODO [from highest to lowest priorities, ish]:
- use yt-dlp python library -> adds more flexibility since the project revolves mostly around python and it's cringe to just run a bash script
- integrate nginx server to Dockerfile -> this will enable the container to serve the videos, which an external reverse proxy can therefore serve
- get rid whitespaces in download file name -> cause whitespaces are stupid and ugly
- add a healthcheck line for Dockerfile
- maybe better logging? -> i don't think the python script catches all the information needed but it catches enough 